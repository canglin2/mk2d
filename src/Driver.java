import java.applet.Applet;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;
import javax.print.DocFlavor.URL;
import javax.swing.ImageIcon;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.opengl.ImageIOImageData;
import org.newdawn.slick.state.StateBasedGame;

import MKUtilities.*;
import Constants.*;

/**
 * Driver.java - Starts the KartsGame
 * 
 * @author Benjamin C. Humphries & Christopher Anglin
 * @version 08-APR-2015
 */
public class Driver extends StateBasedGame{
	
	private static final String gamename = "Mario Kart";
	private static final int menu = 0;
	private static final int play = 1;
	private static final int start = 2;
	private static final int end = 3;
	
	public Driver(String gamename) {
		super(gamename);
		this.addState(new Menu(menu));
		this.addState(new Start(start));
		this.addState(new MKEndGame(end));
		this.addState(KartsGame.KartsGameSharedInstance());	

	}

	public static void main(String[] args)
	{	
		try {
			Display.setIcon(new ByteBuffer[] {
			        new ImageIOImageData().imageToByteBuffer(ImageIO.read(new File("Images/icon.png")), false, false, null),
			        new ImageIOImageData().imageToByteBuffer(ImageIO.read(new File("Images/icon.png")), false, false, null),
			        });
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
		    Class util = Class.forName("com.apple.eawt.Application");
		    Method getApplication = util.getMethod("getApplication", new Class[0]);
		    Object application = getApplication.invoke(util);
		    Class params[] = new Class[1];
		    params[0] = Image.class;
		    Method setDockIconImage = util.getMethod("setDockIconImage", params);
		    Image image = ImageIO.read(new File("Images/icon.png"));;
		    setDockIconImage.invoke(application, image);
		} catch (ClassNotFoundException e) {
		    // log exception
		} catch (NoSuchMethodException e) {
		    // log exception
		} catch (InvocationTargetException e) {
		    // log exception
		} catch (IllegalAccessException e) {
		    // log exception
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MKDebug.log("Starting client");
		
		//KartsGame game = KartsGame.KartsGameSharedInstance("Mario Karts");
		AppGameContainer container = null;
		
		try {
			container= new AppGameContainer(new Driver(gamename), MKConstants.GAME_WIDTH,MKConstants.GAME_HEIGHT,false);
			container.setShowFPS(false); //Delete this line to show FPS on screen
			//container.setIcons( new String[] {"Images/icon.tga", "Images/icon24.tga", "Images/icon16.tga"} );
			container.start();
			
			
			
		} catch (SlickException e) {
			MKDebug.log("Error(AppGameContainer): " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void initStatesList(GameContainer gc) throws SlickException {
		this.getState(menu).init(gc, this);
		this.getState(play).init(gc, this);
		this.getState(start).init(gc, this);
		this.getState(end).init(gc, this);
		this.enterState(start);
	}
}
