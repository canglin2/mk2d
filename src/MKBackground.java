import Constants.MKConstants;
import MKUtilities.MKSize;
import MKUtilities.MKSpriteNode;


public class MKBackground extends MKSpriteNode{

	public MKBackground() {
		super();
		this.setImage("background.png");
		this.setSize(new MKSize(MKConstants.GAME_WIDTH, MKConstants.GAME_HEIGHT));
	}
}
